CFILES=$(wildcard *.c)
OFILES=$(CFILES:.c=.o)
CFLAGS = -Wimplicit-fallthrough -g

bf: $(OFILES)
	$(CC) $(CFLAGS) $< -o $@

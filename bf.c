#include <stdio.h>
#include <string.h>

/* The original implementation had this many memory cells. */
#define MEMORY_SIZE (30000)

/** 'Pointer' memory index. */
int memoryIndex = 0;

/** Memory tape. */
long memory[MEMORY_SIZE] = {0};

enum RunResult {
  SUCCESS = 0,
  IMBALANCED_BRACKETS,
};

/**
 * Interpret a 'file name' to get a FILE* to the program source.
 * Cases:
 *  - : Read the program from stdin, not worrying about conflicting with input.
 *  #... : Use the rest of the string after the # as the program source.
 *  default : Interpret the argument as a file name.
 *
 * @param progId Program identifier. Program-length lifetime assumed.
 * @return The program source, or NULL if no program found.
 */
FILE *getProgram(char *progId) {
  if (progId == NULL) {
    return NULL;
  } else if (progId[0] == '\0') {
    return NULL;
  }

  FILE *program = NULL;

  if (strcmp("-", progId) == 0) {
    /* Allow a program to be read from stdin. */
    puts("Using stdin");
    program = stdin;
  } else if (strncmp(progId, "#", 1) == 0) {
    program = fmemopen(&progId[1], strlen(progId) - 1, "r");
    puts("Using parameter string");
  } else {
    puts("using filename");
    program = fopen(progId, "r");
  }

  return program;
}

/**
 * Interpret a BF program's source code.
 * Will read program until EOF. This may also cause stdin to reach EOF!
 * @param program A non-NULL FILE* in "r" mode.
 */
enum RunResult runProgram(FILE *program) {
  /* The bracket stack is program-local. */
  int stack[256];
  int stackIndex = 0;

  char instruction;
  while ((instruction = getc(program)) != EOF) {
    switch (instruction) {
    case '+':
      memory[memoryIndex]++;
      break;
    case '-':
      memory[memoryIndex]--;
      break;
    case '>':
      memoryIndex++;
      break;
    case '<':
      memoryIndex--;
      break;
    case '.':
      putchar(memory[memoryIndex]);
      break;
    case ',':
      memory[memoryIndex] = getchar();
      break;
    case '[':
      stackIndex++;
      stack[stackIndex] = ftell(program) - 1;
      if (memory[memoryIndex] == 0) {
        int bracketLevel = stackIndex - 1;
        /* Scan forward to find the matching closing bracket. */
        while (stackIndex > bracketLevel) {
          switch (getc(program)) {
          case '[':
            stackIndex++;
            break;
          case ']':
            stackIndex--;
            break;
          case EOF:
            /* Couldn't find the matching closing bracket. */
            puts("Imbalanced brackets.");
            return IMBALANCED_BRACKETS;
          default:
            break;
          }
        }
      }
      break;
    case ']':
      fseek(program, stack[stackIndex], SEEK_SET);
      stackIndex--;
      break;
    default:
      /* Explicitly do nothing with an unrecognised character. */
      break;
    }
  }

  return SUCCESS;
}

int main(int argc, char *argv[]) {

  /* Read and evaluate each file named in argv. */
  for (int fileCount = 1; fileCount < argc; ++fileCount) {

    FILE *program = getProgram(argv[fileCount]);

    if (program != NULL) {
      switch (runProgram(program)) {
      case IMBALANCED_BRACKETS:
	puts("Brackets were imbalanced.");
      default:
	break;
      }
      /* Don't close stdin! */
      if (program != stdin)
        fclose(program);
    }

    /*
     * Subsequent programs may still need to read stdin.
     * It is guaranteed to have EOF flag set if program was read from it.
     */
    clearerr(stdin);
  }
}
